var net = require('net'),
    JsonSocket = require('json-socket');
   	appName = "ROUTE_LOOK_UP",
	port = 10500,
	thru = 0,
	time = 0,
	debug = false,
	client = 11000,
	host = '127.0.0.1',
    server = net.createServer(),
	next = new JsonSocket(new net.Socket()),
	rClient = require('redis').createClient(),
	outbound = [], //We will use this as a queue for our outgoing jobs
	BinaryHeap = require('../common_js/binary_heap').BinaryHeap,
	incoming = new BinaryHeap(scoreTransactions),
	interval = null,
	TIMEOUT = 20000;
     //Decorate a standard net.Socket with JsonSocket
    //;

//We will return scores of 0-100. SMS will always have the lowest score, of 0
function scoreTransactions(transaction){
	if(transaction.type == "SMS"){
		return 100;
	}else{
		return Math.round(10000/transaction.value);
	};
};

server.listen(port,function(){
	//set up the next guy	
	next.connect(client, host);
	console.log('listening');
});


//score by cost only, for now
function scoreNetworks(network){
	//if(network.cost){
		return network.cost;
	//}
	//return 100;
};

next.on('connect',function(){
	if(debug){
		console.log(appName+" the next socket is ready");
	}
});

server.on('connection',function(socket){
	socket = new JsonSocket(socket);
	socket.on('message',function(data){
		//console.log(appName+" queue size "+incoming.size());
		if(data.status){
	        var simulation = data.simulation;
	        if(data.status == "START"){
		        console.log(appName+" received start message");
		        var timestamp = {
		        	time_start : Date.now()
		        }
		        simulation[appName] = timestamp;
		        sendToNext({sim: simulation, status:"START"});
		  	}else if(data.status == "STOP"){
		        console.log(appName+" received end message");
		        //updateSimTimeStamp(data.simulation, "STOP");
		        var timestamp = {
		        	time_end : Date.now()
		        }
		        simulation[appName] = timestamp;
		        sendToNext({sim: simulation, status:"STOP"});
		    }
	    }else{
	    	incoming.push(data);
	    	sendData();
	    };
	    //console.log(thru+" "+appName+": "+data);
	});
})

function route(transaction){
	if((transaction.dest == undefined || transaction.dest == "") || (transaction.type == undefined || transaction.type == "")){
   		var error = appName + ": undefined transaction type or destination";
   		transaction.err = error;
		if(debug){
			console.log(appName + " Error");
			console.log(transaction);
		}
   	}else{
   		var key = transaction.dest + "_" + transaction.type;
   		//returns preferred network, based on cost
		rClient.zrange(key,0,-1,function(err, results){
			if(err){
				transaction.err = appName + ": Error looking up partners";
				if(debug){
					console.log(appName+" Error looking up partners");
					console.log(transaction);					
				}
			}else if(results.length==0){
				transaction.err =appName + ": No network found";
				if(debug){
					console.log(appName + "No network found");
					console.log(transaction);
				}		
			}else{
				var network_heap = new BinaryHeap(scoreNetworks);
				if(debug){
					console.log("Here are the results");
					console.log(results);
				}
				for (var i = results.length - 1; i >= 0; i--) {
					try{
						var network = JSON.parse(results[i]);
					}catch(e){
						console.log(e);
						console.log(network);
						console.log(" While parsing networks");
					}
					network_heap.push(network);
				};
				var network = network_heap.pop();
				//Our heap should be ordered by now.
				transaction.network = network;
				sendToNext(transaction);
			};
		});
   	}
}

function sendData(){
	while(transaction = incoming.pop()){
		route(transaction);
	};
};


function nextNodeRetry(timeout){
	if(timeout >= TIMEOUT ){	
		console.log(appName+" Next Socket is unavailable");
		leaving(0);
	}
	setTimeout(function(){
		if(!next.writable){
			while(data = outbound.shift()){
				next.sendMessage(data);
			}
		}else{
			nextNodeRetry(2*timeout);
		}
	},timeout);
}
// if(debug){
// 			//thru+=1;
// 			//console.log(appName +": " + thru);
// 			//console.log(data);
// 		};
function sendToNext(data){	
	if(debug){
		thru+=1;
		console.log(appName +" ready to send out "+thru);
		console.log(data);
	}
	//socket ready?
	if(!next.writable){
    	next.sendMessage(data);
  	}else{
	   outbound.push(data);
	}
}

process.on('SIGTERM',function(code){
	leaving(code);
});


function leaving(x){
	console.log(appName+" leaving with "+x);
	server.close();
	process.exit();
}

process.on('uncaughtException',function (argument) {
  console.log(appName+": error: "+argument);
  // body...
})

