var net = require('net'),
    JsonSocket = require('json-socket'),
    appName = "TRANSMISSION",
	port = 11000,
	thru = 0,
	time = 0,
	debug = true,
	client = 0,
    host = '127.0.0.1',
    server = net.createServer(),
	rClient = require('redis').createClient(),
	outbound = [],
	interval = null,
	SIMLOG = "SIMLOG"; //Decorate a standard net.Socket with JsonSocket
    //;
server.listen(port,function(){
	//set up the next guy
	//next.connect(client, host);
	console.log('listening');
	set_interval();
	//check the state of the array every half second
});

//check the state of the outbound array and do something about it
function set_interval(){
	console.log("interval is set")
	interval = setInterval(function(){
		if(outbound.length != 0)
			transmit();
	}, 500);
}

server.on('connection',function(socket){
	socket = new JsonSocket(socket);
	socket.on('message',function(data){
		if(debug){	
			//thru+=1;
			//console.log(appName+" received."+thru);
			//console.log(data);
		}
		if(data.status){
			console.log("hhhhhhhhhhhhheeeeeeeeeeeee");
			var simulation = data.sim;
	        if(data.status == "START"){
		        console.log(appName+" received start message");
		        var timestamp = {
		        	time_start : Date.now()
		        }
		        simulation[appName] = timestamp;
		        updateSimTimeStamp(simulation);
		    }
		    else if(data.status == "STOP"){
		        console.log(appName+" received end message");
		        var timestamp = {
		        	time_end : Date.now()
		        }
		        simulation[appName] = timestamp;
		        updateSimTimeStamp(simulation);
		        //sendToNext({sim: simulation, status:"STOP"});
		    }
	    }else{
	    	//
	    	//var transaction = data;
	    	outbound.push(data);
	    }
		//console.log(thru+" "+appName+": "+data);
	});
});

function transmit(){
	//var transaction = outbound.pop();
	while(transaction = outbound.shift()){
		//clearInterval(interval);
		var network = transaction.network.url;
		var partner = new JsonSocket(new net.Socket());
		var phost = network.substring(network.lastIndexOf("/")+1,network.lastIndexOf(":"));
		var port = network.substring(network.lastIndexOf(":")+1);
		if(debug){	
			thru+=1;
			// console.log(thru+" Was gonna send to "+phost+" "+port);
		}
		if((parseInt(port))<15004){
			try{
			partner.connect(phost,port,function(){
				partner.sendSingleMessageAndReceive(transaction,function(err,result){
					console.log("the end");
					console.log(result);
				});
			});
			}catch(e){
				console.log(e);
				console.log(port);
				//console.log("0000000000000000000000000");
			}
		}else{
			//console.log(thru+" Was gonna send to "+phost+" "+port);
			//console.log(transaction);
		}		
	}
	//set_interval();
}

function updateSimTimeStamp(b){
	rClient.sadd(SIMLOG, JSON.stringify(b), function(err, result){
		if(err){
			console.log(err);
		}
	});
}

process.on('SIGTERM',function(code){
	console.log(appName+" leaving with "+code);
	clearInterval(interval);
	server.close();
	process.exit();
});

process.on('uncaughtException',function (argument) {
  console.log(appName+": error: "+argument);
  // body...
})
