/**
 * Module dependencies.
 */
/** Start with 4 args:
 *
 * 0: node
 * 1: app.js
 * 2: <net name>
 * 3: <my_port>
 * 4: best Effort
 * 5: debug
 */

var net = require('net'),
    JsonSocket = require('json-socket'),
    guarantee = false,
    appName = process.argv[2] || "partener_0",
    port = process.argv[3] || 15000,
    debug = false,
    server = net.createServer();

if(process.argv[4] || process.argv[4] == "1"){
	guarantee  = true;
}
if(process.argv[5] == "1"){
	debug = true;
}

server.listen(port,function(){
	console.log(port);
	console.log(appName + 'is ready');
});

server.on('connection',function(socket){
	socket.on('message',function(data){
		//confirm sums and what not, do the do.
		if(guarantee){
			socket.sendSingleMessage({msg:"OK"});
		}
		if(debug){
			console.log(data);
		}
	});
});

process.on('uncaughtException',function (argument) {
  console.log(appName+": error: "+argument);
  // body...
})
