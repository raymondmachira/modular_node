#! /bin/bash
#start_partners.sh
# author: Raymond Macharia <raymond.machira@gmail.com>
# a bash script used to create parners and start them up.
# eats two arguments - the number of partners to generate, and the start port
# exits quietly when both args are not supplied

echo starting $1 partners from port $2

bestEffort="1"
name="partner_"
start_port=15000
debug="1"

#array_countries = (KEN,CHI,TUR,IND);
#
#((n=0;n<5;n++))
for ((i=0;i<$1;i++))	#in $1
	# random cost between 0.0000 and 15.0000
	#cost=$[100 + (RANDOM % 100)]$[1000 + (RANDOM % 1000)]
	#cost=$[RANDOM % 15].${cost:1:2}${cost:4:3}
	do
		port=$(($start_port+$i))
		echo $port
		nodejs partners/app.js $name$i $port $bestEffort $debug &
	done
exit 0