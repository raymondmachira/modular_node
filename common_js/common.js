
function Transaction(){
	this.sim_name= "";
	this.value = 0;
	this.type = "SMS";
	this.src ="NYC";
	this.msg="qwerty";
	this.dest="";
	this.time_gen=0;
};

function Simulation(){
	this.num_transactions = 100,
	this.sms_propn = 50,
	this.ken_sms = 25,
	this.min_fin = 1000,
	this.max_fin = 500,
 	this.ind_sms = 25,
 	this.chi_sms = 25,
 	this.tur_sms = 25,
 	this.ken_fin = 25,
 	this.ind_fin = 25,
 	this.tur_fin = 25,
 	this.chi_fin = 25,
 	this.sim_name = "ren_ren";
}

exports.Transaction = Transaction;
exports.Simulation = Simulation;