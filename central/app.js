var net = require('net'),
    JsonSocket = require('json-socket')
    rClient = require('redis').createClient(),
    appName = "NYC_GATEWAY"
    server = net.createServer(),
    SIMLOG = "SIMLOG";
    thru = 0,
    client = 10500,
    port = 10000, //The same port that the server is listening on
    host = '127.0.0.1',
    debug = false,
    next = new JsonSocket(new net.Socket()), //Decorate a standard net.Socket with JsonSocket
    outbound = {};

server.listen(port);
server.on('listening',function(){
  next.connect(client, host);
  // if(!receipts){
  //   console.log('receipts is empty')
  // }
  console.log('listening');
});
  server.on('connection',function (argument) {
  console.log(appName+' listening at '+port);
  socket = new JsonSocket(argument);
  socket.on('message',function(data){
    if(debug){
      thru+=1;
      console.log(thru+' '+appName +': ');
      console.log(data);
    }
    //InterProcessMessage --> marks the last message
    if(data.status){
        var simulation = data.simulation;
        if(data.status == "START"){
	        console.log(appName+" received start message");
	        //simulation.time_end = Date.now();
	        var timestamp = {
	        	time_start : Date.now()
	        }
	        simulation[appName] = timestamp;
	        sendToNext({sim: simulation, status:"START"});
	        //updateSimTimeStamp(data.simulation, "STOP");
	    }
	    else if(data.status == "STOP"){
	        console.log(appName+" received end message");
	        //updateSimTimeStamp(data.simulation, "STOP");
	        var timestamp = {
	        	time_end : Date.now()
	        }
	        simulation[appName] = timestamp;
	        sendToNext({sim: simulation, status:"STOP"});
	        //
	    }
    }
    sendToNext(data);    
    //s
  })
})


function sendToNext(data){
  //if()
  //console.log(next);
  if(!next.writable){
  		next.sendMessage(data);     	
  }else{
      outbound.push(data);
  }

  // next.on('connect',function(){
  //   console.log("after connect ");
  //   console.log(next);
    
  // })
  //console.log(appName + 'sendToNext');
}

// function updateSimTimeStamp(name,stamp){
//   console.log("---here----");
//   rClient.smembers(SIMLOG,function(err,result){
//     if(err){
//       console.log(err);
//     }
//     for (var i = result.length - 1; i >= 0; i--){
//       var b = JSON.parse(result[i]);
//       console.log("here is b");
//       console.log(b);
//       console.log("name is "+name);
//       if(name == b.sim_name){
//         console.log("found");
//         rClient.srem(SIMLOG,result[i],function(err,result){
//           if(err){
//             console.log(err);
//           }
//           console.log("removed "+result);
//           //update the object with the new time
//           //b.generation.stop = Date.now();
//           b[appName][stamp] = Date.now();

//           //reinsert.
//           rClient.sadd(SIMLOG, JSON.stringify(b), function(err, result){
//             if(err){
//               console.log(err);
//             }
//           });
//         });
//       }
//     };
//   })
// }


// function updateSimTimeStamp(name,stamp){
//   rClient.smembers(SIMLOG,function(err,result){
//     if(err){
//       console.log(err);
//     }
//     for(var i = result.length - 1; i >= 0; i--){
//       var b = JSON.parse(result[i]);
//       if(name == b.sim_name){
//         rClient.srem(SIMLOG,result[i],function(err,result){
//           if(err){
//             console.log(err);
//           }
//           //update the object with the new time
          
//           //reinsert.
//           rClient.sadd(SIMLOG, b, function(err, result){
//             if(err){
//               console.log(err);
//             }
//           });
//         });
//         break;
//       };
//     };
//   })
// }


process.on('SIGTERM',function(code){
  console.log(appName+" leaving with "+code);
  server.close();
  process.exit();
});
// process.on('SIGTERM',function(code){
//   console.log(appName+" leaving with "+code);
//   server.close(function(){
//     server.on('close',function(){
//       process.exit();
//     })
//   });
// })

// process.on('error',function (argument) {
//   console.log(appName+": error: "argument);
//   // body...
// })

process.on('uncaughtException',function (argument) {
  console.log(appName+": error: "+argument);
  // body...
})
