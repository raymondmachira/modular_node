var net = require('net'),
    JsonSocket = require('json-socket'),
    rClient = require('redis').createClient(),
	//express = require('express'),
	appName = "TRANSACTION_GENERATOR",
	common = require('../common_js/common'),
    Transaction = common.Transaction,
    SIMLOG = "SIMLOG",
    sim = common.Simulation;
    simulation = new sim(),
    port = 10000,
	host = '127.0.0.1',
	myPort = 9500,
	server = net.createServer(),
	socket = new JsonSocket(new net.Socket),
	queue = [],
	debug = false,
	socketState = null;


//our server and it's callbacks
server.listen(myPort,function(){
	//console.log("-----------------Before Write-------------------");
	//console.log(socket);
	socket.connect(port, host);
	// rClient.get(appName,function(err,result){
	// 	if(err){
	// 		console.log("error setting error Client: "+appName+" : "+err);
	// 		process.exit(0);
	// 	}
	// 	try{
	// 		var indx = result.lastIndexOf(":") + 1;
	// 		// JSON.parse(result);
	// 		socket.connect(result.substring(indx), host);
	// 	}catch(e){
	// 		console.log(appName+" error parsing redis result: "+e);
	// 		throw e;
	// 	}		
	// });
});

function setup_transmissions(){
 	if(debug){
 		console.log(appName+": "+"max_fin: "+simulation.max_fin);
 		console.log(appName+": "+"min_fin: "+simulation.min_fin);
 	}
 	simulation.sim_name += "_"+Date.now();
 	var ken_sms = simulation.ken_sms * 1.00;
 	var ind_sms = simulation.ind_sms * 1.00;
 	var chi_sms = simulation.chi_sms * 1.00;
 	var tur_sms = simulation.tur_sms * 1.00;
 	var ken_fin = simulation.ken_fin * 1.00;
 	var ind_fin = simulation.ind_fin * 1.00;
 	var tur_fin = simulation.tur_fin * 1.00;
 	var chi_fin = simulation.chi_fin * 1.00;
 	var sim_name = simulation.sim_name;
 	//console.log("."+simulation.ind_fin+".");
 	//console.log("sms total: "+simulation.ken_sms+0.00+simulation.ind_sms+simulation.chi_sms+simulation.tur_sms);
	//console.log("fin total: "+simulation.ken_fin+0.00+simulation.ind_fin+simulation.chi_fin+simulation.tur_fin);
		
	if((ken_sms+ind_sms+chi_sms+tur_sms) != 100 || (ken_fin+ind_fin+chi_fin+tur_fin != 100)){
	    console.log("sms: "+ken_sms);
	    console.log(ind_sms);
	    console.log(chi_sms);
	    console.log(tur_sms);
	    console.log("fin: "+ken_fin);
	    console.log(ind_fin);
	    console.log(chi_fin);
	    console.log(tur_fin);
		//res.render('index.html',{error: 'Shouldn\'t proportions add up to 100%?'});
		return;
	}else {
		var num_sms_trans = simulation.num_transactions * (simulation.sms_propn / 100)
		var num_fin_trans = simulation.num_transactions - num_sms_trans;
		var min_fin = simulation.min_fin*1.0;
		var max_fin = simulation.max_fin*1.0;
		var diff = max_fin-min_fin;
		if(debug){
			console.log(appName+": "+"num_sms_trans: "+num_sms_trans);
			console.log(appName+": "+"num_fin_trans: " +num_fin_trans);
		}
		//financial transactions
		var chi_fin = num_fin_trans*0.01*simulation.chi_fin,ken_fin = num_fin_trans*0.01*simulation.ken_fin,tur_fin = num_fin_trans*0.01*simulation.tur_fin,ind_fin = num_fin_trans*0.01*simulation.ind_fin;
		if(debug){
			console.log("chi_fin: "+chi_fin);
		}
		actually_send(simulation,"CHI","FIN",Math.round(chi_fin),max_fin,min_fin);
		actually_send(simulation,"IND","FIN",Math.round(ind_fin),max_fin,min_fin);
		actually_send(simulation,"KEN","FIN",Math.round(ken_fin),max_fin,min_fin);
		actually_send(simulation,"TUR","FIN",Math.round(tur_fin),max_fin,min_fin);
		
		//SMS Transaction
		var chi_sms = num_sms_trans*0.01*simulation.chi_fin,ken_sms = num_sms_trans*0.01*simulation.ken_sms,tur_sms = num_sms_trans*0.01*simulation.tur_sms,ind_sms = num_sms_trans*0.01*simulation.ind_sms;
		if(debug){
			console.log("chi_sms: "+chi_sms);
		}
		actually_send(simulation,"CHI","SMS",Math.round(chi_sms),max_fin,min_fin);
		actually_send(simulation,"IND","SMS",Math.round(ind_sms),max_fin,min_fin);
		actually_send(simulation,"KEN","SMS",Math.round(ken_sms),max_fin,min_fin);
		actually_send(simulation,"TUR","SMS",Math.round(tur_sms),max_fin,min_fin); 		
		//res.render('index.html',{error:''})
	}	
}
function notifyTransmissionStatus(sim,status){
	console.log(appName+": "+status);
	//console.log(status);
	send({simulation: sim, status:status});
}

function actually_send(sim, country, type, num, max, min){
	//Must be the beginning of the transmissions
	//var i = 0;
	if((country == "CHI") && (type == "FIN")){
		var generation = {
			time_start : Date.now()
		};
		simulation[appName] = generation;
		// rClient.sadd(SIMLOG,JSON.stringify(simulation),function(err,result){
		// 	if(err){
		// 		console.log(err);
		// 	}
		// });
		//send({"msg":"START"});
		notifyTransmissionStatus(sim,"START");
	}
	if((type=="SMS") && (country =="TUR") && (num == 0)){
		//updateSim(name);
		sim[appName].time_end = Date.now();
		notifyTransmissionStatus(sim, "STOP");
		//send({"msg":"END"});
	}
	for(var i = 0; i < num; i++){
		var transaction = new Transaction();
		transaction.dest = country;
		transaction.sim_name = sim.sim_name;
		//transaction.time_gen = Date.now();
		if(type == "FIN"){
			transaction.type = type;
			var value = (min + Math.random()*(max-min)).toFixed(4);
			transaction.value = value;
		}
		if(debug){
			console.log(transaction);
		}
		//last transaction
		//console.log(appName+" posting to: "+JSON.stringify(client.url));			
		send(transaction);
		if((type=="SMS") && (country =="TUR") && (i == num-1)){
			//updateSimTimeOut(name);
			sim[appName].time_end = Date.now();
			notifyTransmissionStatus(sim, "STOP");		
			//send({"msg":"END"});
		}

		// client.post("/transaction",{transaction: transaction},function(err,req,res,obj){
		// 	//req.end();
		// 	// if(err && debug){
		// 	// 	console.log(appName+": "+err+"\n");
		// 	// };
		// });		 

		// collection.insert(transaction,function(err,result){
		// 	if(err){
		// 		console.log(err);
		// 	}
		// });
	}
}
/***The Simulation Object***/
function send(Transaction){
	if(!socket.writable){
		socket.sendMessage(Transaction);
	}else{
		queue.push(Transaction);
	}
}
// function updateSimTimeOut(name){
// 	rClient.smembers(SIMLOG,function(err,result){
// 		if(err){
// 			console.log(err);
// 		}
// 		for (var i = result.length - 1; i >= 0; i--){
// 			var b = JSON.parse(result[i]);
// 			// console.log("here is b");
// 			// console.log(b);
// 			if(name == b.sim_name){
// 				rClient.srem(SIMLOG,result[i],function(err,result){
// 					if(err){
// 						console.log(err);
// 					}
// 					//update the object with the new time
// 					b[appName]["STOP"] = Date.now();

// 					//reinsert.
// 					rClient.sadd(SIMLOG, JSON.stringify(b), function(err, result){
// 						if(err){
// 							console.log(err);
// 						}

// 					});
// 				});
// 			}
// 		};
// 	})
// }
socket.on('connect', function(){ //Don't send until we're connected
  	console.log("The socket is ready");
  	//Observe the state of the socket, and the array every ten seconds
  	// setInterval(function(){
  	// 	console.log("-------------------------");
  	// 	console.log("queue length");
  	// 	console.log(queue.length);
  	// 	//console.log(socket);
  	// },10000);
  	setup_transmissions();
});

// server.on('connection', function(socket) { //This is a standard net.Socket
//     socket = new JsonSocket(socket); //Now we've decorated the net.Socket to be a JsonSocket
//     socket.on('message', function(message) {
//         var result = message.a + message.b;
//         socket.sendEndMessage({result: result});
//     });
// });

process.on('SIGTERM',function(code){
	console.log(appName+" leaving with "+code);
	server.close();
	process.exit();
});

process.on('uncaughtException',function (argument) {
	console.log(appName+": error: "+argument);
	// body...
})

